# Online backup through rsync, marking repositories as read-only (experimental)

This experimental script automates backing up repositories (while marking them as read-only for safety) through `rsync` directly on-disk to avoid duplicating deduplicated objects in pool repositories - https://gitlab.com/gitlab-org/gitlab/-/issues/328295 has the full context and reasoning.

## Process

1. Back up any repositories outside of fork networks, while marking them as read-only during the process
1. For each fork network:
    1. Mark all projects inside read-only
    1. Find the pool repository, back up if present
    1. Back up all projects in the fork network
    1. Revert the read-only access

## Assumptions

- You're running a single-node Omnibus instance, or have direct disk access to all shards from the Rails node running this snippet
- You have `rsync` installed
- You have a full, working backup before the first time you run this, to validate the result as this is an experimental script

## How to use

1. **WARNING**: Before running this script, please make sure you have a recent, full backup in order to validate the results.
1. Download the script:
   ```shell
   curl https://gitlab.com/gitlab-org/geo-team/runbooks/-/raw/main/experimental-online-backup-through-rsync/backup_readonly_rsync.rb -o /tmp/backup_readonly_rsync.rb
   ```
1. Open `/tmp/backup_readonly_rsync.rb` with your editor, and edit the `BACKUP_MAPPING` section. It should contain one entry per each shard as configured in `/etc/gitlab/gitlab.rb` under `git_data_dirs`.
1. Run the script with:

   ```shell
   sudo gitlab-rails runner /tmp/backup_readonly_rsync.rb
   ```

   This would start the process described above, and as a result the repositories should get backed up into the `destination_path` configured for each shard.
